const express = require(`express`);
const mongoose = require(`mongoose`);
const dotenv = require(`dotenv`).config();
const cors= require(`cors`);

const PORT = process.env.PORT || 3008;
const app = express();

// Connect the routes module
const userRoutes = require(`./routes/userRoutes`);
const courseRoutes = require(`./routes/courseRoutes`);

// Middlewares to handle json payloads
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors())     // prevents blocking of request from client especially different domains

// Connect database to server
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

// Test DB connection
const db = mongoose.connection;
db.on("error", console.error.bind(console, 'connection error:'));
db.once("open", () => console.log(`Connected to Database`));

// Schema
    // /models/User.js

// Routes 
    // create a middleware to be the root url of all routes
app.use(`/api/users`, userRoutes);
app.use(`/api/courses`, courseRoutes);

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`));

