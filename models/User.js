const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, `First name is required`]
    },
    lastName: {
        type: String,
        required: [true, `Last name is required`]
    },
    email: {
        type: String,
        required: [true, `Email is required`],
        unique: true
    },
    password: {
        type: String,
        required: [true, `Password is required`]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    enrollments: [                  // each element in this array will comply with this schema
        {
            courseId: {
                type: String,
                require: [true, `Course ID is required`],
            },
            status: {
                type: String,
                default: "Enrolled"
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
}, {timestamps: true})

// Export the model
module.exports = mongoose.model(`User`, userSchema);


