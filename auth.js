const jwt = require(`jsonwebtoken`)

// sign method user
module.exports.createToken = (data) => {
    let userData = {
        id: data._id,
        email: data.email,
        isAdmin: data.isAdmin
    }
    
    // sign method creates the token
    return jwt.sign(userData, process.env.SECRET_PASS)
}

// verify method users
module.exports.verify = (req, res, next) => {
    const requestToken = req.headers.authorization
    // console.log(requestToken)
    
    if(typeof requestToken == "undefined"){
        res.status(401).send(`Token missing`)
    } else {
        const token = requestToken.slice(7, requestToken.length)
        // console.log(token)
        if(typeof token != "undefined"){
            return jwt.verify(token, process.env.SECRET_PASS, (err, data) => {
                if(err){
                    return res.send({auth: `Auth Failed!`})
                } else {
                    next()
                }
            })
        }
    }
}

// decode method users
module.exports.decode = (bearerToken) => {
    // console.log(bearerToken)
    const token = bearerToken.slice(7, bearerToken.length)
    // console.log(token)
    return jwt.decode(token)
}


// verify isAdmin status method
module.exports.verifyAdmin = (req, res, next) => {
	const requestToken = req.headers.authorization
	// console.log(requestToken)
	if(typeof requestToken == "undefined"){
		res.status(401).send(`Token missing`)
	}else{
		const token = requestToken.slice(7, requestToken.length);
		// console.log(token)
		if(typeof token !== "undefined"){
			const admin = jwt.decode(token).isAdmin
			if(admin){
				return jwt.verify(token, process.env.SECRET_PASS, (err, data) => {
					if(err){
						return res.send({auth: `auth failed!`})
					} else{
						next()
					}
				})
			} else {
				res.status(403).send(`You are not authorized.`)
			}
		}
	}
}

