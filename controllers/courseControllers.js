const Course = require (`./../models/Course`)

// CREATE NEW COURSE
module.exports.create = async (reqBody) => {
    const {courseName, description, price} = reqBody
    let newCourse = new Course({
        courseName: courseName,
        description: description,
        price: price
    })
    return await newCourse.save().then((result, err) => result ? true : err)
}

// // GET ALL COURSES
module.exports.getAllCourses = async () => {
    return await Course.find().then(result => result)
}

// RETRIEVE COURSE INFO
module.exports.courseInfo = async (id) => {
    return await Course.findById(id).then((result, err) => {
        if(result){
            return result
        } else{
            if(result == null){
                return {message: `User does not exist`}
            } else {
                return err
            }
        }
    })
}

// UPDATE COURSE INFO
module.exports.update = async (courseId, reqBody) => {
    // console.log(reqBody)
    const courseData = Course({
        course: reqBody.courseName,
        description: reqBody.description,
        price: reqBody.price
    })
    return await Course.findByIdAndUpdate(courseId, {$set: courseData}, {new: true})
    .then((result, err) => {
        if(result) {
            return result
        } else {
            return err
        }
    })
}

// UPDATE ISACITVE TO FALSE
module.exports.courseArchive = async (courseId) => {
	return await Course.findOneAndUpdate(courseId, {$set: {isActive: false}}, {new:true})
    .then((result, err) => result ? true : err)
}

// UPDATE ISACITVE TO TRUE
module.exports.courseUnarchive = async (courseId) => {
	return await Course.findOneAndUpdate(courseId, {$set: {isActive: true}}, {new:true})
    .then((result, err) => result ? true : err)
}

// GET ALL ACTIVE COURSE
module.exports.getAllActive = async () => {
    return await Course.find({isActive: true})
    .then((result, err) => {
        if(result){
            return true
        } else {
            if(result == null){
                return false
            } else {
                return err
            }
        }
    })
}