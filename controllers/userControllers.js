const User = require (`./../models/User`)
const CryptoJS = require("crypto-js");
// const bcrypt = require('bcrypt');
const {createToken} = require(`./../auth.js`);
// const res = require("express/lib/response");
// const { findOneAndUpdate } = require("./../models/User");
// const req = require("express/lib/request");

// GET ALL USERS
module.exports.getAllUsers = async () => {
    return await User.find().then(result => result)
}

// REGISTER A USER
module.exports.register = async (reqBody) => {
    // console.log(reqBody)
    const {firstName, lastName, email, password} = reqBody
    
    const newUser = new User({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
        // password: bcrypt.hashSync(password, 10)
    })
    return await newUser.save().then(result => {
        // return result
        if (result) {
            return true
        } else {
            if(result == null){
                return false
            }
        }
    })
}

// CHECK IF EMAIL EXISTS
module.exports.checkEmail = async (reqBody) => {
    const {email} = reqBody
    return await User.findOne({email: email}).then((result, err) => {
        if(result){
            return true
        } else {
            if(result == null){
                return false
            } else {
                return err
            }
        }
    })
}

// LOGIN A USER
module.exports.login = async (reqBody) => {
    return await User.findOne({email: reqBody.email}).then((result, err) => {
        if(result == null){
            return {message: `User does not exist`}
        } else {
            
            if(result!= null){
                // check if password is correct
                const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
                console.log(reqBody.password == decryptedPw)
                if(reqBody.password == decryptedPw){
                    //create a token for the user
                    // console.log(`Created token`)
                    return {token: createToken(result) }
                } else {
                    return {auth: `Authentication Failed!`}
                }
            } else {
                return err
            }
        }
        })
}

// RETRIEVE USER INFORMATION
module.exports.profile = async (id) => {
    return await User.findById(id).then((result, err) => {
        if(result){
            return result
        } else{
            if(result == null){
                return {message: `User does not exist`}
            } else {
                return err
            }
        }
    })
}

// UPDATE USER INFO
module.exports.update = async (userId, reqBody) => {
    // console.log(reqBody)
    const userData = {
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
    }
    return await User.findByIdAndUpdate(userId, {$set: userData}, {new: true})
    .then((result, err) => {
        if(result) {
            result.password = "***"
            return result
        } else {
            return err
        }
    })
}

// UPDATE USER PASSWORD
module.exports.updatePassword = async (userId, reqBody) => {
    const userPw = {
        password: CryptoJS.AES.encrypt(reqBody, process.env.SECRET_PASS).toString()
    }
    return await User.findByIdAndUpdate(userId, {$set: userPw}, {new: true})
    .then((result, err) => {
        if(result) {
            result.password = "***"
            return result
        } else {
            return err
        }
    })
}

// UPDATE ISADMIN STATUS TO TRUE
module.exports.adminStatus = async (reqBody) => {
	const {email} = reqBody
	return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err)
}

// UPDATE ISADMIN STATUS TO FALSE
module.exports.userStatus = async (reqBody) => {
    const {email} = reqBody
    return await User.findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin: false}}, {new: true})
    .then((result,err) => result ? true : err)
}

// DELETE A USER
module.exports.deleteUser = async (reqBody) => {
    const {email} =reqBody
    return await User.findOneAndDelete({email:email}.then((result,err) => result ? true : err))
}