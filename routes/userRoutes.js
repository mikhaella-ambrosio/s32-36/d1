const express = require(`express`);
const router = express.Router();

// Import units of functions from userController module
const {
    register,
    getAllUsers,
    checkEmail,
    login,
    profile,
    update,
    updatePassword,
    adminStatus,
    userStatus,
    deleteUser
} = require(`./../controllers/userControllers`)

const {verify, decode, verifyAdmin} = require(`./../auth`);     // import verify from auth.js file bc request is from client
// const { route } = require("express/lib/application");

// GET ALL USERS
router.get(`/`, async (req, res) => {
    try {
        await getAllUsers().then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

// REGISTER A USER
router.post(`/register`, async (req, res) => {
    // console.log(req.body)
    
    try {
        await register(req.body).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

// CHECK IF EMAIL ALREADY EXISTS 
router.post(`/email-exists`, async (req, res) => {
    try{
       await checkEmail(req.body).then(result => res.send(result))
    } catch (err){
        res.status(500).json(err)
    }
})

// LOGIN A USER
    // authentication
router.post(`/login`, async (req, res) => {
    // console.log(req.body)
    try {
       await login(req.body).then(result => res.send(result))
    } catch (err) {
        res.status(500).json(err)
    }
})

// RETRIEVE USER INFORMATION
    // user document
    // verify first before executing async function
router.get(`/profile`, verify, async (req, res) => {
    // res.send(`Welcome to GET request`)
    const userId = decode(req.headers.authorization).id
    // console.log(userId)
    // console.log(req.headers.authorization)
    try{
        await profile(userId).then(result => res.send(result))
    } catch (err){
        res.status(500).json(err)
    }
})

// UPDATE USER INFORMATION
router.put(`/update`, verify, async (req, res) => {
    const userId = decode(req.headers.authorization).id
    try{
        await update(userId, req.body).then(result => res.send(result))
    } catch (err){
        res.status(500).json(err)
    }
})

// UPDATE USER PASSWORD
router.patch(`/user-password`, verify, async (req, res) => {
    const userId = decode(req.headers.authorization).id
    try{
        await updatePassword(userId, req.body.password).then(result => res.send(result))
    } catch (err){
        res.status(500).json(err)
    }
})

// UPDATE USER'S ISADMIN STATUS TO TRUE
    // only admin can update user's isAdmin status
// ** OPTION 1 **  
// router.patch(`/isAdmin`, verify, async (req, res) => {
//     // console.log(req.body)
//     // console.log( decode(req.headers.authorization).isAdmin)
//     const admin = decode(req.headers.authorization).isAdmin
//     try {
//         if(admin == true){
//             await adminStatus(req.body).then(result => res.send(result))
//         } else{
//             res.send(`You are not authorized!`)
//         }
//     } catch (err){
//         res.status(500).json(err)
//     }
// })

// ** OPTION 2 ** 
router.patch('/isAdmin', verifyAdmin, async (req, res) => {
	try{
		await adminStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

// UPDATE USER'S ISADMIN STATUS TO FALSE
    // only admin can update user's isAdmin status
router.patch(`/isUser`, verifyAdmin, async (req, res) => {
    try{
        await userStatus(req.body).then(result => res.send(result))
    } catch(err){
        		res.status(500).json(err)
        	}
})

// DELETE A USER
// create a route to delete a user from the database, make sure that the route is secured
    // stretch goal: only admin can delete a user
router.delete(`/delete-user`, verifyAdmin, async (req,res) => {
    try {
        await deleteUser(req.body).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})





// export this module to be used in index.js
module.exports = router;