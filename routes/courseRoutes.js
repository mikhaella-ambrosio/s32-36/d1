const express = require(`express`);
const router = express.Router();

const {
    create,
    getAllCourses,
    courseInfo,
    courseArchive,
    courseUnarchive,
    getAllActive
} = require (`./../controllers/courseControllers`)

const {verify, verifyAdmin} = require(`./../auth`);

// CREATE NEW COURSE
router.post(`/create`, verifyAdmin, async (req, res) => {
    // console.log(req.body)
    try {
        await create(req.body)
        .then(result => console.log(result))
        res.send(result)
    } catch(err){
        res.status(500).json(err)
    }
})

// GET ALL COURSES
router.get(`/`, async (req, res) => {
    try {
        await getAllCourses().then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

// RETRIEVE COURSE INFO
router.get(`/:courseId`, verify, async (req, res) => {
    try{
        await courseInfo(req.params.courseId).then(result => res.send(result))
    } catch (err){
        res.status(500).json(err)
    }
})

// UPDATE COURSE INFO
router.put(`/:courseId/update`, verifyAdmin, async (req, res) => {
    try{
        await update(req.params.courseId, req.body).then(result => res.send(result))
    } catch (err){
        res.status(500).json(err)
    }
})


// UPDATE ISACTIVE STATUS TO FALSE
router.patch(`/:courseId/archive`, verifyAdmin, async (req, res) => {
    try{
        await courseArchive(req.body).then(result => res.send(result))
    } catch(err){
        		res.status(500).json(err)
        	}
})

// UPDATE ISACTIVE STATUS TO true
router.patch(`/:courseId/unArchive`, verifyAdmin, async (req, res) => {
    try{
        await courseUnarchive(req.body).then(result => res.send(result))
    } catch(err){
        		res.status(500).json(err)
        	}
})

// GET ALL ACTIVE COURSES 
router.get(`/`, async (req, res) => {
    try {
        await getAllActive()
        .then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})






// export this module to be used in index.js
module.exports = router;